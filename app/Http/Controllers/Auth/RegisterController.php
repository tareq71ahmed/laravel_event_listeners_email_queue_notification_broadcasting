<?php

namespace App\Http\Controllers\Auth;

use App\Events\NewCustomerHasRegistration;
use App\Mail\VerificationEmail;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Queue;
use Illuminate\Support\Facades\Mail;
use Carbon\Carbon;


class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
//    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
       $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'email_verification_token'=>str_random(40),
        ]);



//       Mail::to($user->email)->queue(new VerificationEmail($user));
        // return redirect()->to('login')->with('message','Email Must Be Verified');
        return redirect()->back();



    }




    public function register(Request $request)
    {
        $this->validator($request->all())->validate();
        event(new Registered($user = $this->create($request->all())));
        return redirect(route('register'));
    }



    protected function registered(Request $request, $user)
    {
        Auth::logout();
        return redirect(route('register'));
    }






    public function verifyEmail($token=null)
    {
        if($token == null)
        {
            session()->flash('type','warning');
            session()->flash('message','Invalid Token');
            return redirect()->route('login');
        }
        $user = User::where('email_verification_token',$token)->first();
//        if($token == null)
//        {
//            session()->flash('type','warning');
//            session()->flash('message','Invalid Token');
//            return redirect()->route('login');
//        }


        $usercount = User::where('email_verification_token',$token)->count();
        if($usercount == 1)
        {

            $user->update([
                'email_verified'=>1,
                'status'=>1,
                'email_verified_at'=>Carbon::now(),
                'email_verification_token'=>'',
            ]);

            session()->flash('type','success');
            session()->flash('message','Account Activated Successfully!');
            return redirect()->route('login');

        }else
        {
            session()->flash('type','warning');
            session()->flash('message','Account Already Activated.You can login Now!');
            return redirect()->route('login');

        }


//        session()->flash('type','success');
//        session()->flash('message','Invalid Token');
        return redirect()->route('login');

    }








}
