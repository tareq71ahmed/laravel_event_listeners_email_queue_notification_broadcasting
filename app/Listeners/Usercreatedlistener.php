<?php

namespace App\Listeners;

use App\Mail\VerificationEmail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class Usercreatedlistener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        $email = $event->user->email ;

//        Mail::to($event->user->email)->queue(new VerificationEmail($user));
        Mail::to($email)->queue(new VerificationEmail($event->user));

    }
}
