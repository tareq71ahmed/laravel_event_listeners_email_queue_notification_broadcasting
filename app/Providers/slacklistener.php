<?php

namespace App\Providers;

use App\Events\NewCustomerHasRegistration;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class slacklistener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewCustomerHasRegistration  $event
     * @return void
     */
    public function handle(NewCustomerHasRegistration $event)
    {
        //
    }
}
